Diaporama réalisé par XXXX.

Modèle proposé par l'[AEIF](https://aeif.fr), hébergé sur la [Forge des Communs Numérique de l'Éducation](https://forge.apps.education.fr/docs/modeles/diaporama-en-markdown/)

<!--

Choisissez ci-dessous la licence de votre diaporama
en ajoutant un x entre les crochets de votre choix :

[ ] Tout droit réservé
[ ] CC BY NC ND
[ ] CC BY ND
[ ] CC BY NC SA
[X] CC BY SA	
[ ] CC BY NC	
[ ] CC BY	
[ ] CC ZERO	

Pour savoir quelle licence choisir, consultez ce tableau :
https://link.infini.fr/licencescc

La licence choisie sera automatiquement affichée dans
l'onglet "A propos" du menu de votre diaporama.

-->
